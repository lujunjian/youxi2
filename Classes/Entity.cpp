//
//  Entity.cpp
//  youxi2
//
//  实体基类
//
//  Created by 路俊建 on 16/5/4.
//
//

#include "Entity.hpp"

Entity::Entity(){

    m_sprite = NULL;

};

Entity::~Entity(){


};


// 获取精灵
Sprite* Entity::getSprite(){

    return this->m_sprite;

};

// 绑定精灵
void Entity::bindSprite(Sprite* sprite){

    
    this->m_sprite = sprite;
    this->addChild(m_sprite);
    
    Size size = m_sprite->getContentSize();
    m_sprite->setPosition(Point(size.width * 0.5f, size.height * 0.5f));
    this->setContentSize(size);
    
};