//
//  Player.hpp
//  youxi2
//
//  Created by 路俊建 on 16/5/4.
//
//

#ifndef Player_hpp
#define Player_hpp

#include "cocos2d.h"
#include "Entity.hpp"

using namespace cocos2d;

class Player : public Entity{

public:
    
    Player();
    
    ~Player();

    virtual bool init();

    CREATE_FUNC(Player);
    
    // 跳跃躲避
    void jump();
    
    void run();
    
    // 创建一个自定义奔跑动作
    cocos2d::Animate * createRunAction();
   
    
    // 收到伤害
    void hit();
    
    // 获取血量
    int getHP();
    
    
    void resetData();
    
    
private:
    
    // 是否跳跃
    bool m_isJumping;

    // 血量
    int m_iHP;
};

#endif /* Player_hpp */
