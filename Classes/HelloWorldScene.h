#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "Player.hpp"


USING_NS_CC;
using namespace cocos2d::ui;


class HelloWorld : public cocos2d::Layer
{
public:
    
    static cocos2d::Scene* createScene();
    
    virtual bool init();

    CREATE_FUNC(HelloWorld);
   
	void loadPlayer();

    void loadControlBetton();

	void loadBackground();

	void loadMonster();

	void loadActionUI();

	void loadHP();
    
    virtual void update(float dt);
    
    void jumpEvent(Ref* ref, TouchEventType type);
    
  

private:
    

   
    // 背景1
    Sprite* m_bgSprite1;
   
    // 背景2
    Sprite* m_bgSprite2;
   
    // 主角
    Player* m_player;
    
    Label* m_hpNumber;

	Size visibleSize;

};

#endif // __HELLOWORLD_SCENE_H__
