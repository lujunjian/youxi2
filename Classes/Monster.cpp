//
//  Monster.cpp
//  youxi2
//
//  Created by 路俊建 on 16/5/8.
//
//

#include "Monster.hpp"

Monster::Monster(){


    this->is_activity = false;
    
};

Monster::~Monster(){



};


bool Monster::init(){



    return true;

};


void Monster::show(){

    
    // 设置可见
    setVisible(true);
    
    this->is_activity = true;
    
};

void Monster::hide(){

    setVisible(false);
   
    reset();
    
    this->is_activity = false;

};

void Monster::reset(){


    // 设置坐标随机
    setPosition(Vec2(800 + CCRANDOM_0_1() * 2000, 500 - CCRANDOM_0_1() * 1000 ));

};

bool Monster::isActivity(){

    return this->is_activity;

};

bool Monster::isCollideWithPlayer(Player* player){


    // 返回玩家精灵的矩形
    Rect entityRect = player->getBoundingBox();

    Vec2 monsterVerc2 = getPosition();

   
    // 判断玩家矩形与怪物中心点是否交叉
    return entityRect.containsPoint(monsterVerc2);

};




