//
//  Player.cpp
//  youxi2
//
//  Created by 路俊建 on 16/5/4.
//
//

#include "Player.hpp"

#include "SimpleAudioEngine.h"

Player::Player(){
    
    this->m_isJumping = false;
    
    // 初始化血量
    this->m_iHP = 50;

};

Player::~Player(){

};

bool Player::init(){

    return true;
};

void Player::jump(){
    
    // 防止连环跳
    if (this->m_isJumping) {
        
        return;
    }
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("jump.mp3");
   
    this->m_isJumping = true;
    
   
    auto jump = JumpBy::create(1.0f, Vec2(0,0), 200, 1);
    
    auto callFunc = CallFunc::create([&](){
    
        this->m_isJumping = false;
    
    });
    
    // 创建连续动作
    auto jumpActions = Sequence::create(jump,callFunc, NULL);
    
    this->runAction(jumpActions);

}

void Player::run(){

    this->m_sprite->runAction(createRunAction());

}

/* 创建奔跑动作 */
Animate* Player::createRunAction(){
    
    int iFrameNum = 15;
    
    SpriteFrame* frame = NULL;
    Vector<SpriteFrame*> frameVec;
    
    for (int i = 1; i <= iFrameNum; i++) {
        
        frame = SpriteFrame::create(StringUtils::format("animation2/run%d.png",i), Rect(0,0,130,130));
        
        frameVec.pushBack(frame);
        
    }
    
    Animation* animation = Animation::createWithSpriteFrames(frameVec);
    
    animation->setLoops(-1);
    
    animation->setDelayPerUnit(0.03f);
    
    Animate* action = Animate::create(animation);
    
    return action;
    
};

void Player::hit(){

    // 每次受到伤害减少15滴血
    m_iHP -= 15;
    
    if (m_iHP < 0) {
        m_iHP = 0;
    }
   
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("hit.mp3");
    
    // 被击中后给点动作
    auto backMove = MoveBy::create(0.2f, Vec2(-40, 0));
    
    auto ForwadAction = MoveBy::create(0.2f, Vec2( 40, 0));

    // 组合动作
    auto actions = Sequence::create(backMove,ForwadAction, NULL);
    
    // 先停止所有动作
    stopAllActions();
   
    resetData();
    
    // 执行动作
    runAction(actions);
}

int Player::getHP(){

    return this->m_iHP;
}

void Player::resetData(){

    if (this->m_isJumping) {
        
        this->m_isJumping = false;
    }
    
    setPosition(Vec2(200,140));
}


