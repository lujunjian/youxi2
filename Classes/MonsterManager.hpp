//
//  MonsterManager.hpp
//  youxi2
//
//  Created by 路俊建 on 16/5/8.
//
//

#ifndef MonsterManager_hpp
#define MonsterManager_hpp
#include "cocos2d.h"
#include "Monster.hpp"
#include "Player.hpp"

USING_NS_CC;

#define MAX_SHOW_MONSTER_NUM 10  // 最大显示怪物数量

class MonsterManager : public Node{


public:
    
    virtual bool init();
    
    virtual void update(float dt);
    
    CREATE_FUNC(MonsterManager);
    
    void bindPlayer(Player* player);

private:
    
    void createMonsters();  // 创建怪物对象

    Vector<Monster*> m_monsterArr; // 存放怪物对象
    
    Player* m_player;
    
};

#endif /* MonsterManager_hpp */
