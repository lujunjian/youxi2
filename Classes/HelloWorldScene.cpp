#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

#include "Player.hpp"
#include "MonsterManager.hpp"

USING_NS_CC;
using namespace cocos2d::ui;


using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    auto scene = Scene::create();
    
    auto layer = HelloWorld::create();

    scene->addChild(layer);

    return scene;
}

bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
	visibleSize = Director::getInstance()->getVisibleSize();

    // 开启update方法
//	this->scheduleUpdate();
 
	// 载入关卡背景
//	loadBackground();

	// 载入猪脚
//	loadPlayer();

	// 载入怪物
//	loadMonster();

	// 载入游戏控制杆
	loadControlBetton();

	// 载入动作UI,如跳跃按钮等
//	loadActionUI();

	// 载入血量
//	loadHP();

    return true;
}


void HelloWorld::loadPlayer() {

	// 创建主角
	this->m_player = Player::create();
	this->m_player->bindSprite(Sprite::create("animation2/run1.png"));
	this->m_player->setPosition(Point(200, visibleSize.height / 4));
	this->addChild(this->m_player, 1);
	// 执行奔跑动作
	this->m_player->run();
}


void HelloWorld::loadControlBetton(){

	// 控制杆背景
    auto m_ctrl_bg = Sprite::create("control_bg.png");
	m_ctrl_bg->setPosition(100, 100);
    this->addChild(m_ctrl_bg,4);

    
	// 控制球
    auto m_ctrl_btn = Sprite::create("control_btn.png");
	auto size = m_ctrl_btn->getContentSize();
    m_ctrl_btn->setPosition(Vect(size.width + m_ctrl_bg->getPositionX() / 2, size.height  + m_ctrl_bg->getPositionY() /2));
    this->addChild(m_ctrl_btn,4);


	auto listener1 = EventListenerTouchOneByOne::create();//创建一个触摸监听  
	listener1->setSwallowTouches(true);//设置是否想下传递触摸  

									   //3.0 后可以直接在touchBegan后添加它的实现代码，而不用特意去写一个touchBegan的函数  
	listener1->onTouchBegan = [](Touch* touch, Event* event) {
		auto target = static_cast<Sprite*>(event->getCurrentTarget());//获取的当前触摸的目标  

		Point locationInNode = target->convertToNodeSpace(touch->getLocation());
		Size s = target->getContentSize();
		Rect rect = Rect(0, 0, s.width, s.height);

		if (rect.containsPoint(locationInNode))//判断触摸点是否在目标的范围内  
			return true;
		else
			return false;
	};

	//拖动精灵移动  
	listener1->onTouchMoved = [=](Touch* touch, Event* event) {
		auto target = static_cast<Sprite*>(event->getCurrentTarget());
/*		auto target_size = target->getContentSize();
		if (target_size.width + target->getPositionX() >= m_ctrl_bg->getPositionX()) {
			target->setPositionX(m_ctrl_bg->getPositionX() + target_size.width);
		}

		if (target_size.height + target->getPositionY() >= m_ctrl_bg->getPositionY()) {
			target->setPositionY(m_ctrl_bg->getPositionY()+ target_size.height);
		}
		*/
		target->setPosition(target->getPosition() + touch->getDelta());
	};

	listener1->onTouchEnded = [=](Touch* touch, Event* event) {
	};
	//将触摸监听添加到eventDispacher中去  
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener1, m_ctrl_btn);

}


void HelloWorld::loadMonster() {
	// 创建怪物
	MonsterManager* monsterManager = MonsterManager::create();
	monsterManager->bindPlayer(this->m_player);
	this->addChild(monsterManager, 4);
}


void HelloWorld::loadBackground() {

	// 初始化背景1
	this->m_bgSprite1 = Sprite::create("bg1.png");

	// 设置锚点,这句话不设置默认也就是0.5 0.5,也就是交叉点在图片的正中心
	this->m_bgSprite1->setAnchorPoint(Point(0.5, 0.5));

	// 设置坐标
	this->m_bgSprite1->setPosition(Point(visibleSize.width / 2, visibleSize.height / 2));
	this->addChild(this->m_bgSprite1, 0);


	// 初始化背景2,将其追加到背景1后面
	this->m_bgSprite2 = Sprite::create("bg2.png");

	// 设置锚点,这句话不设置默认也就是0.5 0.5,也就是交叉点在图片的正中心
	this->m_bgSprite2->setAnchorPoint(Point(0.5, 0.5));

	Size mapSize = this->m_bgSprite2->getContentSize();

	// 设置坐标
	this->m_bgSprite2->setPosition(Point(visibleSize.width / 2 + mapSize.width, visibleSize.height / 2));

	//设置水平翻转
	this->m_bgSprite2->setFlippedX(true);

	this->addChild(this->m_bgSprite2, 0);

}

void HelloWorld::loadActionUI() {

	// 添加跳跃按钮
	Button* jumpbtn = Button::create("button.png");
	jumpbtn->setPosition(Vec2(visibleSize.width - 100, 100));
	this->addChild(jumpbtn, 1);
	jumpbtn->addTouchEventListener(this, toucheventselector(HelloWorld::jumpEvent));
}

void HelloWorld::loadHP() {

	// 初始化血量ui
	auto hpLable = Label::createWithSystemFont("HP:", "", 15);
	hpLable->setPosition(Vec2(40, visibleSize.height - 40));
	this->addChild(hpLable, 4);
	this->m_hpNumber = Label::createWithSystemFont(Value(this->m_player->getHP()).asString(), "", 15);
	this->m_hpNumber->setPosition(Vec2(80, visibleSize.height - 40));
	this->addChild(this->m_hpNumber, 4);

}

void HelloWorld::update(float dt) {

	// 获取两个背景图片的坐标
	int pox1 = this->m_bgSprite1->getPositionX();
	int pox2 = this->m_bgSprite2->getPositionX();

	// 速度
	int speed = 4;

	pox1 -= speed;
	pox2 -= speed;

	Size mapSize = this->m_bgSprite1->getContentSize();

	// 无线滚动算法,注意这里的－mapsize.width是负的,也就是表示图片完全移出去
	if (pox1 <= -mapSize.width / 2) {
		pox1 = mapSize.width + mapSize.width / 2;
	}

	if (pox2 <= -mapSize.width / 2) {
		pox2 = mapSize.width + mapSize.width / 2;
	}

	this->m_bgSprite1->setPositionX(pox1);
	this->m_bgSprite2->setPositionX(pox2);

	// 更新显示的血量
	this->m_hpNumber->setString(Value(this->m_player->getHP()).asString());
}

void HelloWorld::jumpEvent(Ref* ref, TouchEventType type) {
	switch (type) {

	case TOUCH_EVENT_ENDED:

		this->m_player->jump();
		break;

	default:
		break;
	}
};