//
//  MonsterManager.cpp
//  youxi2
//
//  Created by 路俊建 on 16/5/8.
//
//

#include "MonsterManager.hpp"
#include "Player.hpp"


bool MonsterManager::init(){
   
    createMonsters();
    
    this->scheduleUpdate();

    return true;
}

void MonsterManager::update(float dt){

    
    for (auto monster : this->m_monsterArr) {
        
        
        if ( monster->isActivity() ) {
            
            
            monster->setPositionX(monster->getPositionX() - 10);
            
            
            if (monster->getPositionX() < 0) {
           
                monster->hide();
   
            }else if( monster->isCollideWithPlayer(m_player)){
            
                m_player->hit();
           
                monster->hide();
            
            }
        
        } else {
        
            monster->show();
        
        }
        
    }


}

void MonsterManager::createMonsters(){
   
    Monster* monster = NULL;
    
    for (int i = 0 ; i < MAX_SHOW_MONSTER_NUM; i++) {
       
        monster = Monster::create();
        
        monster->bindSprite(Sprite::create("monster2.png"));
        
        monster->reset();
        
        this->addChild(monster);
        
        this->m_monsterArr.pushBack(monster);
        
    }

}

void MonsterManager::bindPlayer(Player* player){

    this->m_player = player;
}
