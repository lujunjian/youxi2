//
//  Monster.hpp
//  youxi2
//
//  Created by 路俊建 on 16/5/8.
//
//

#ifndef Monster_hpp
#define Monster_hpp
#include "cocos2d.h"
#include "Entity.hpp"
#include "Player.hpp"
USING_NS_CC;

class Monster:public Entity{

public:
    
    Monster();
    
    ~Monster();
    
    CREATE_FUNC(Monster);
    
    virtual bool init();
    
public:
    
    void show(); // 显示怪物
    
    void hide(); // 隐藏怪物
    
    void reset(); // 重置怪物数据
    
    bool isActivity();  // 是否活动
    
    // 检测是否碰撞
    bool isCollideWithPlayer( Player* player);
    
private:
    bool is_activity;  
    

};



#endif /* Monster_hpp */
