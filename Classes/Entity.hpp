//
//  Entity.hpp
//  youxi2
//
//  Created by 路俊建 on 16/5/4.
//
//

#ifndef Entity_hpp
#define Entity_hpp
#include "cocos2d.h"

USING_NS_CC;

class Entity : public Node{


public:
    
    Entity();
    ~Entity();
    Sprite * getSprite();
    void bindSprite(Sprite * sprite);
    
    Sprite* m_sprite;
    
private:

};


#endif /* Entity_hpp */
